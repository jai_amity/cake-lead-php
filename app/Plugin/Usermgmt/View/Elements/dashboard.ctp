<?php
/*
	This file is part of UserMgmt.

	Author: Chetan Varshney (http://ektasoftwares.com)

	UserMgmt is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	UserMgmt is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
?>

<?php   if ($this->UserAuth->isAdmin()) { ?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
	
    <!-- Brand and toggle get grouped for better mobile display -->
    <!--<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>-->

    <!-- Collect the nav links, forms, and other content for toggling -->
	
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		<li style="float:left"  class="active"><?php echo $this->Html->link(__("Dashboard",true),"/dashboard") ?></li>
		<?php   if ($this->UserAuth->isAdmin()) { ?>
			<li><?php echo $this->Html->link(__("Add User",true),"/addUser") ?></li>
			<li><?php echo $this->Html->link(__("All Users",true),"/allUsers") ?></li>
			<li><?php echo $this->Html->link(__("Add Group",true),"/addGroup") ?></li>
			<li><?php echo $this->Html->link(__("All Groups",true),"/allGroups") ?></li>
			<li><?php echo $this->Html->link(__("Permissions",true),"/permissions") ?></li>
			<li><?php echo $this->Html->link(__("Profile",true),"/viewUser/".$this->UserAuth->getUserId()) ?></li>
		<?php   } else {?>
			<li><?php echo $this->Html->link(__("Profile",true),"/myprofile") ?></li>
		<?php   } ?>						
      </ul>						
    </div><!-- /.navbar-collapse -->
	
  </div><!-- /.container-fluid -->
</nav>
<?php   } ?>